import os
import hashlib
import argparse
from collections import defaultdict

def get_files(input_folder_path):
    # Create a dictionary to store file names as keys and a set of paths as values
    files_dict = defaultdict(set)

    # Recursively walk through the input folder path
    for root, dirs, files in os.walk(input_folder_path):
        for file_name in files:
            file_path = os.path.join(root, file_name)
            # Add the file path to the set of paths associated with the file name
            files_dict[file_name].add(file_path)

    return files_dict  # file_name: {path_1, path_2, ...}

def search_for_duplicates_by_name(files_dict):
    # Create a dictionary to store file names as keys and a set of paths as values
    duplicates_dict = defaultdict(set)

    # Iterate over the file names and paths in the files_dict
    for file_name, file_paths in files_dict.items():
        if len(file_paths) > 1:
            # If there are multiple paths associated with the same file name,
            # add all the paths to the duplicates_dict for that file name
            duplicates_dict[file_name] = file_paths

    return duplicates_dict  # file_name: {path_1, path_2, ...}

def search_for_duplicates_by_name_and_size(files_dict):
    # Get the duplicates by file name using the search_for_duplicates_by_name function
    duplicates_by_name = search_for_duplicates_by_name(files_dict)
    # Create a dictionary to store file traits (name, size) as keys and a set of paths as values
    duplicates_dict = defaultdict(set)

    # Iterate over the file names and paths in the duplicates_by_name dictionary
    for file_name, file_paths in duplicates_by_name.items():
        file_sizes = defaultdict(set)

        # Iterate over the file paths with the same file name
        for file_path in file_paths:
            # Get the size of the current file path
            file_size = os.path.getsize(file_path)
            # Add the file path to the set of paths associated with the file size
            file_sizes[file_size].add(file_path)

        # Iterate over the file sizes and paths in the file_sizes dictionary
        for file_size, paths in file_sizes.items():
            if len(paths) > 1:
                # If there are multiple paths associated with the same file name and size,
                # add all the paths to the duplicates_dict for that file name and size
                duplicates_dict[(file_name, file_size)] = paths

    return duplicates_dict  # (file_name, file_size): {path_1, path_2, ...}

def calculate_hash(file_path, hashing_algorithm):
    # Create a new hasher object based on the specified hashing algorithm
    hasher = hashlib.new(hashing_algorithm)

    # Open the file in binary mode
    with open(file_path, 'rb') as file:
        while True:
            # Read the file data in chunks of 4096 bytes
            file_data = file.read(4096)

            if not file_data:
                break

            # Update the hasher with the file data
            hasher.update(file_data)

        # Return the hexadecimal representation of the calculated hash
        return hasher.hexdigest()

def search_for_duplicates_by_name_size_and_hash(files_dict, hashing_algorithm):
    # Get the duplicates by name and size using the search_for_duplicates_by_name_and_size function
    duplicates_by_name_and_size = search_for_duplicates_by_name_and_size(files_dict)
    # Create a dictionary to store file traits (name, size, hash) as keys and a set of paths as values
    duplicates_dict = defaultdict(set)

    # Iterate over the file traits and paths in the duplicates_by_name_and_size dictionary
    for file_traits, file_paths in duplicates_by_name_and_size.items():
        # Get the file name, size, and hash from the file_traits tuple
        file_name = file_traits[0]
        file_size = file_traits[1]

        # Iterate over the file paths with the same file name, size, and hash
        for file_path in file_paths:
            # Calculate the hash of the current file path using the specified hashing algorithm
            file_hash = calculate_hash(file_path, hashing_algorithm)

            if file_hash is not None:
                # Add the file path to the set of paths associated with the file traits (name, size, hash)
                duplicates_dict[(file_name, file_size, file_hash)].add(file_path)

    return duplicates_dict  # (file_name, file_size, file_hash): {path_1, path_2, ...}

# The remaining functions (search_for_duplicates_by_name_size_and_bit, search_for_duplicates_by_name_size_bit_and_hash,
# search_for_duplicates_by_hash, search_for_duplicates_by_bit) are placeholders and not implemented in the code.

def search_for_duplicates(files_dict, mode, hashing_algorithm):
    # Create a dictionary to store the duplicates
    duplicates_dict = defaultdict(set)

    # Check the value of the mode argument and call the corresponding function
    match mode:
        case 'n':       # name
            duplicates_dict = search_for_duplicates_by_name(files_dict)
        case 'ns':      # name and size
            duplicates_dict = search_for_duplicates_by_name_and_size(files_dict)
        case 'nsh':     # name, size, and hash
            duplicates_dict = search_for_duplicates_by_name_size_and_hash(files_dict, hashing_algorithm)
        case _:
            print("Select a mode.")

    return duplicates_dict

def write_to_file(duplicates_dict, output_file_path):
    # Open the output file in write mode
    with open(output_file_path, 'w') as output_file:
        # Iterate over the file traits and paths in the duplicates_dict
        for file_traits, file_paths in duplicates_dict.items():
            # Write the file traits as a header
            output_file.write(f"{file_traits}:\n")

            # Iterate over the file paths and write each path
            for file_path in file_paths:
                output_file.write(f"\t{file_path}\n")

    print(f"The output has been saved to '{output_file_path}'")

# Create an ArgumentParser object to parse the command-line arguments
parser = argparse.ArgumentParser()

# Add the command-line options
parser.add_argument('-i', '--input', type=str, help='the input folder in which to search for duplicated files')
parser.add_argument('-o', '--output', type=str, default='output.txt', help='path to the output file')
parser.add_argument('-m', '--mode', choices=['n', 'ns', 'nsh'], type=str, default='ns', help='search mode to use, between name, name and size, name, size and hash')
parser.add_argument('--hash', choices=['md5', 'sha1', 'sha224', 'sha256', 'sha384', 'sha512', 'sha3_224', 'sha3_256', 'sha3_384', 'sha3_512'], type=str, default='md5', help='hashing algorithm to use when comparing files')

# Parse the command-line arguments
args = parser.parse_args()
input_folder_path = args.input
output_file_path = args.output
mode = args.mode
hashing_algorithm = args.hash

if input_folder_path is None:
    parser.error("Provide the input folder path using the '-i' or '--input' option.")

# Get the files in the input folder
files_dict = get_files(input_folder_path)

# Search for duplicates based on the specified mode and hashing algorithm
duplicates_dict = search_for_duplicates(files_dict, mode, hashing_algorithm)

# Write the duplicates to the output file
write_to_file(duplicates_dict, output_file_path)
